use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result;
use std::io::Error;

#[derive(Debug)]
pub struct FileError {
    kind: String,
    message: String,
}

impl Display for FileError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "File Error:{{error kind: {}, error message: {}}}",
            self.kind, self.message
        )
    }
}

impl From<Error> for FileError {
    fn from(error: Error) -> Self {
        FileError {
            kind: String::from("io_error"),
            message: error.to_string(),
        }
    }
}

impl From<&mut Error> for FileError {
    fn from(error: &mut Error) -> Self {
        FileError {
            kind: String::from("io_error"),
            message: error.to_string(),
        }
    }
}

impl FileError {
    pub fn create_error(kind: &str, message: &str) -> Option<FileError> {
        Some(FileError {
            kind: String::from(kind),
            message: String::from(message),
        })
    }
}
