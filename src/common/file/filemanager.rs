use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;

use crate::common::file::fileerror::FileError;

pub struct FileManager {
    path: Option<String>,
    file: Option<File>,
}

impl FileManager {
    pub fn new(file_path: String) -> FileManager {
        FileManager {
            path: Some(file_path),
            file: None,
        }
    }

    pub fn open_append(&mut self, append: bool) -> bool {
        let mut result = false;
        if let Some(path) = &self.path {
            println!("path: {:?}", path);
            let mut _file = OpenOptions::new().append(append).open(path);
            match _file {
                Ok(f) => {
                    println!("ok");
                    self.file = Some(f);
                    result = true;
                }
                Err(_e) => {
                    println!("failed to open file {:?}", _e);
                    result = false;
                }
            }
        }
        result
    }

    pub fn file_size(&self) -> u64 {
        if let Some(f) = &self.file {
            return f.metadata().unwrap().len();
        }
        return 0 as u64;
    }

    pub fn create_open(&mut self) -> bool {
        let mut result = false;
        let is_exist = self.open_append(false);
        if let Some(path) = &mut self.path {
            if is_exist {
                let _ = fs::remove_file(&path);
            }
            let _file = File::create(path);
            match _file {
                Ok(f) => {
                    println!("ok");
                    self.file = Some(f);
                    result = true;
                }
                Err(_e) => {
                    println!("failed to create file {:?}", _e);
                    result = false;
                }
            }
        }
        result
    }

    pub fn write_string(&mut self, content: &str) -> bool {
        let mut result = false;
        if let Some(real_file) = &mut self.file {
            let _ = real_file.write(content.as_bytes());
            result = true;
        }
        result
    }

    pub fn read_string(&mut self) -> Result<String, FileError> {
        let mut buf = String::new();
        if let Some(path) = &self.path {
            let mut file = OpenOptions::new().read(true).open(path);
            match &mut file {
                Ok(_file) => {
                    let size = _file.read_to_string(&mut buf);
                    println!("read result : {:?}", size);
                    match size {
                        Ok(_size) => {
                            return Ok(buf);
                        }
                        Err(_e) => {
                            println!("read error: {:?}", _e);
                            return Err(std::convert::From::from(_e));
                        }
                    }
                }
                Err(_e) => {
                    println!("open error: {:?}", _e);
                    return Err(std::convert::From::from(_e));
                }
            }
        }
        Err(FileError::create_error("404", "file not exist or not open").unwrap())
    }
}
