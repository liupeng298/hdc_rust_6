use crate::common::buffer::BMap;
use crate::common::buffer::{PtyMap, PtyTask};
use crate::common::config::*;
use crate::common::hdcfile::HdcFile;
use crate::common::hsession::*;
use crate::serializer::packet_assemble;
use crate::serializer::serialize::Serialization;
use crate::serializer::session_struct;

use std::io::{Error, ErrorKind, Read};
use std::process::Command;
use std::sync::Arc;
use tokio::sync::Mutex;

use tokio::io::{self, AsyncReadExt, AsyncWriteExt};
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};
use tokio::net::{TcpListener, TcpStream};

pub async fn daemon_start() -> io::Result<()> {
    let saddr = "0.0.0.0:60000";
    let listener = TcpListener::bind(saddr).await?;
    tracing::info!("bind on {saddr}");
    loop {
        let (stream, addr) = listener.accept().await?;
        tracing::info!("spawn begin client {addr}");
        tokio::spawn(handle_client(stream));
        tracing::info!("spawn end {addr}");
    }
}

async fn read_frame(rd: &mut OwnedReadHalf, expeted_size: usize) -> io::Result<Vec<u8>> {
    if expeted_size == 0 {
        return Ok(vec![]);
    }
    let mut data = vec![0; expeted_size];
    let mut index: usize = 0;
    while index < expeted_size {
        let recv_size = rd.read(&mut data[index..]).await?;
        index += recv_size;
    }
    Ok(data)
}

async fn unpack_task_message(rd: &mut OwnedReadHalf) -> io::Result<TaskMessage> {
    let data = read_frame(rd, packet_assemble::HEAD_SIZE).await?;
    let payload_head = packet_assemble::unpack_payload_head(data)?;
    tracing::info!("get payload_head: {:#?}", payload_head);

    let expected_head_size = u16::from_be(payload_head.head_size) as usize;
    let expected_data_size = u32::from_be(payload_head.data_size) as usize;
    if expected_head_size + expected_data_size == 0
        || expected_head_size + expected_data_size > HDC_BUF_MAX_SIZE
    {
        return Err(Error::new(ErrorKind::Other, "Packet size incorrect"));
    }

    let data = read_frame(rd, expected_head_size).await?;
    let payload_protect = packet_assemble::unpack_payload_protect(data)?;
    tracing::info!("get payload_protect: {:#?}", payload_protect);
    let channel_id = payload_protect.channel_id;
    let try_command = HdcCommand::try_from(payload_protect.command_flag);
    if try_command.is_err() {
        return Err(Error::new(ErrorKind::Other, "unknown command"));
    }
    let command = try_command.unwrap();

    let payload = read_frame(rd, expected_data_size).await?;
    Ok(TaskMessage {
        channel_id,
        command,
        payload,
    })
}

pub fn execute_cmd(cmd: String) -> Vec<u8> {
    let result = Command::new(SHELL_PROC).args(["-c", &cmd]).output();

    match result {
        Ok(output) => [output.stdout, output.stderr].concat(),
        Err(e) => e.to_string().into_bytes(),
    }
}

async fn daemon_unity_execute(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    let cmd = String::from_utf8(task_message.payload).unwrap();
    let data = execute_cmd(cmd);

    let message = TaskMessage {
        channel_id: task_message.channel_id,
        command: HdcCommand::KernelEchoRaw,
        payload: data,
    };
    let _ = BMap::put(session_id, message).await?;

    let message = TaskMessage {
        channel_id: task_message.channel_id,
        command: HdcCommand::KernelChannelClose,
        payload: vec![1],
    };
    let _ = BMap::put(session_id, message).await?;

    Ok(())
}

async fn daemon_file_send(task_message: TaskMessage, session_id: u32) {}

async fn daemon_shell_init(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    tracing::debug!("get shell init payload: {:#?}", task_message.payload);
    let pty_task = PtyTask::new(session_id, task_message.channel_id);
    PtyMap::put(task_message.channel_id, pty_task).await;
    Ok(())
}

async fn daemon_shell_data(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    tracing::warn!("get shell data payload: {:#?}", task_message.payload);
    if let Some(pty_task) = PtyMap::get(task_message.channel_id).await {
        tracing::debug!("get shell data pty");
        let handle = &pty_task.handle;
        tracing::debug!("handle is finished: {}", handle.is_finished());
        for byte in task_message.payload.iter() {
            if *byte == 0x4_u8 {
                handle.abort();
                PtyMap::del(task_message.channel_id).await;
                let message = TaskMessage {
                    channel_id: task_message.channel_id,
                    command: HdcCommand::KernelChannelClose,
                    payload: vec![1],
                };
                let _ = BMap::put(session_id, message).await?;
                break;
            }
            let _ = pty_task.tx.send(*byte).await;
        }
        return Ok(());
    }
    Err(Error::new(ErrorKind::Other, "invalid channel id"))
}

async fn daemon_channel_close(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    if task_message.payload[0] > 0 {
        let message = TaskMessage {
            channel_id: task_message.channel_id,
            command: HdcCommand::KernelChannelClose,
            payload: vec![task_message.payload[0] - 1],
        };
        return BMap::put(session_id, message).await;
    }
    Ok(())
}

async fn filetask(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    let opt = admin_session(ActionType::Query(session_id)).await;
    if opt.is_none() {
        admin_session(ActionType::Add(HdcSession::new(
            session_id,
            String::from(""),
            NodeType::Daemon,
            ConnectType::Tcp,
        )))
        .await;
    }
    let opt = admin_session(ActionType::Query(session_id)).await;

    let arc = opt.unwrap();
    let mut session = arc.lock().await;
    if !session.map_tasks.contains_key(&task_message.channel_id) {
        session.map_tasks.insert(
            task_message.channel_id,
            Arc::new(Mutex::new(HdcFile::new(
                session_id,
                task_message.channel_id,
            ))),
        );
    }
    let task = session.map_tasks.get(&task_message.channel_id).unwrap();
    let task_ = &mut task.lock().await;
    task_
        .command_dispatch(
            task_message.command,
            &task_message.payload,
            task_message.payload.len() as u16,
        )
        .await;

    Ok(())
}

async fn dispatch_task(task_message: TaskMessage, session_id: u32) -> io::Result<()> {
    match task_message.command {
        HdcCommand::ShellInit => {
            tracing::info!("ShellInit");
            return daemon_shell_init(task_message, session_id).await;
        }
        HdcCommand::ShellData => {
            tracing::info!("ShellData");
            return daemon_shell_data(task_message, session_id).await;
        }
        HdcCommand::UnityExecute => {
            tracing::info!("UnityExecute, payload: {:#?}", task_message.payload);
            return daemon_unity_execute(task_message, session_id).await;
        }
        HdcCommand::KernelChannelClose => {
            tracing::info!("KernelChannelClose");
            return daemon_channel_close(task_message, session_id).await;
        }
        HdcCommand::FileCheck
        | HdcCommand::FileData
        | HdcCommand::FileBegin
        | HdcCommand::FileFinish => {
            tracing::info!("FileCheck:{}", task_message.command as u32);
            return filetask(task_message, session_id).await;
        }
        _ => return Err(Error::new(ErrorKind::Other, "unknown command flag")),
    }
}

fn get_hostname(filepath: &str) -> String {
    let mut file = std::fs::File::open(filepath).unwrap();
    let mut hostname = String::new();
    let _ = file.read_to_string(&mut hostname);
    tracing::info!("get host name {hostname}");
    hostname.trim().to_string()
}

async fn handshake(rd: &mut OwnedReadHalf, wr: &mut OwnedWriteHalf) -> io::Result<u32> {
    let task_message = unpack_task_message(rd).await?;
    if let Ok(HdcCommand::KernelHandshake) = HdcCommand::try_from(task_message.command) {
        let mut recv = session_struct::SessionHandShake::default();
        let _ = recv.parse(task_message.payload)?;

        tracing::info!("recv handshake: {:#?}", recv);
        if recv.banner != HANDSHAKE_MESSAGE {
            return Err(Error::new(ErrorKind::Other, "Recv server-hello failed"));
        }

        let send = session_struct::SessionHandShake {
            session_id: 0,
            connect_key: "".to_string(),
            buf: get_hostname(HOSTNAME_FILE),
            auth_type: AuthType::OK as u8,
            ..recv
        };

        tracing::info!("send handshake: {:#?}", send);
        let data = send.serialize();
        let pack = packet_assemble::concat_pack(
            task_message.channel_id,
            HdcCommand::KernelHandshake,
            data,
        );
        wr.write_all(pack.as_slice()).await?;
        BMap::start(recv.session_id).await;

        Ok(recv.session_id)
    } else {
        Err(Error::new(ErrorKind::Other, "unknown command flag"))
    }
}

pub async fn handle_client(stream: TcpStream) -> io::Result<()> {
    let (mut rd, mut wr) = stream.into_split();
    let session_id: u32 = handshake(&mut rd, &mut wr).await?;

    let recv_task = tokio::spawn(async move {
        loop {
            match unpack_task_message(&mut rd).await {
                Ok(task_message) => {
                    if let Err(e) = dispatch_task(task_message, session_id).await {
                        tracing::error!("dispatch task err: {}", e.to_string());
                    }
                }
                Err(e) => {
                    tracing::warn!("unpack task error: {}", e.to_string());
                }
            }
        }
    });

    let send_task = tokio::spawn(async move {
        loop {
            if let Some(message) = BMap::get(session_id).await {
                let data = packet_assemble::concat_pack(
                    message.channel_id,
                    message.command,
                    message.payload,
                );
                let _ = wr.write_all(data.as_slice()).await;
                let _ = wr.flush().await;
            }
        }
    });
    let _ = recv_task.await;
    let _ = send_task.await;

    Ok(())
}
