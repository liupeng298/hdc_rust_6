use crate::common::buffer::BMap;
use async_trait::async_trait;
use std::fs::metadata;

use crate::common::config::CompressType;
use crate::common::config::HdcCommand;
use crate::common::file::filemanager::FileManager;
use crate::common::hdctransfer::HdcTransferBase;
use crate::common::hsession::TaskMessage;
use crate::serializer::serialize::Serialization;

use super::base::Base;
use super::taskbase::TaskBase;
use crate::serializer::transfer_struct::TransferConfig;

pub struct HdcFile {
    pub transfer: HdcTransferBase,
}

impl HdcFile {
    pub fn new(_session_id: u32, _channel_id: u32) -> Self {
        Self {
            transfer: HdcTransferBase::new(_session_id, _channel_id),
        }
    }
}

impl HdcFile {
    pub async fn begin_transfer(&mut self, command: &String) -> bool {
        println!("begin transfer, command: {}", command);
        let mut argc: u32 = 0;
        let argv = Base::split_command_to_args(command, &mut argc);
        for s in &argv {
            println!("{}", s);
        }
        if argc < 2 {
            return false;
        }
        if !self.set_master_parameters(&command, argc, argv) {
            return false;
        }
        self.transfer.is_master = true;
        let local_path = self.transfer.local_path.clone();
        println!("local path111:{}", local_path);
        let mut file_manager = FileManager::new(local_path.clone());
        let open_result = file_manager.open_append(true);
        println!("open_append: {} {:?}", local_path.clone(), open_result);
        if open_result {
            println!("open ok 222");
            let mut config = &mut self.transfer.transfer_config;
            println!("get config ok");
            config.file_size = u64::from(file_manager.file_size());
            config.optional_name = self.transfer.local_name.clone();
            println!(
                "option name:{}, file_size:{}",
                config.optional_name, config.file_size
            );
            if config.hold_timestamp {}

            config.path = self.transfer.remote_path.clone();
            self.check_master(&self.transfer).await;
            return true;
        } else {
            // logMsg
            self.task_finish();
        }

        false
    }

    pub fn set_master_parameters(
        &mut self,
        _command: &String,
        argc: u32,
        argv: Vec<String>,
    ) -> bool {
        println!("setmaster parameters");
        let mut i: usize = 0;
        let mut src_argv_index = 0u8;
        println!("setmaster parameters 222");
        while i < argc as usize {
            match &argv[i] as &str {
                "-z" => {
                    self.transfer.transfer_config.compress_type = CompressType::Lz4 as u8;
                    src_argv_index += 1;
                }
                "-a" => {
                    self.transfer.transfer_config.hold_timestamp = true;
                    src_argv_index += 1;
                }
                "-sync" => {
                    self.transfer.transfer_config.update_if_new = true;
                    src_argv_index += 1;
                }
                "-m" => {
                    self.transfer.is_file_mode_sync = true;
                    src_argv_index += 1;
                }
                "-remote" => {
                    src_argv_index += 1;
                }
                "-cwd" => {
                    src_argv_index += 1;
                    self.transfer.transfer_config.client_cwd = argv.get(i + 1).unwrap().clone();
                }
                _ => {}
            }
            i += 1;
        }
        if argc as u8 == src_argv_index {
            return false;
        }
        println!("set master 333");
        self.transfer.remote_path = argv.get(argv.len() - 1).unwrap().clone();
        self.transfer.local_path = argv.get(argv.len() - 2).unwrap().clone();
        self.transfer.server_or_daemon = true;
        if self.transfer.server_or_daemon {
            if (src_argv_index + 1) as u32 == argc {
                println!("there is no remote path");
                return false;
            }
            let cwd = self.transfer.transfer_config.client_cwd.clone();
            println!("cwd:{}, local path:{}", cwd, self.transfer.local_path);
            self.transfer.local_path = self
                .transfer
                .extract_relative_path(&cwd, &self.transfer.local_path);
            println!("222 cwd:{}, local path:{}", cwd, self.transfer.local_path);
        } else {
            if u32::from(src_argv_index + 1) == argc {
                self.transfer.remote_path = String::from(".");
                self.transfer.local_path = argv.get((argc - 1) as usize).unwrap().clone();
            }
        }
        println!("local path: {}", &self.transfer.local_path);
        println!("remote path: {}", &self.transfer.remote_path);
        self.transfer.local_name = Base::get_full_file_path(&mut self.transfer.local_path).unwrap();
        println!("local name: {}", &self.transfer.local_name);
        let file = metadata(self.transfer.local_path.clone()).unwrap();
        if file.is_dir() {
            println!("is directory: {}", &self.transfer.local_path);
            self.transfer.is_dir = true;
            self.transfer.task_queue = self.transfer.get_sub_files_resurively(
                self.transfer.local_path.clone(),
                self.transfer.local_name.clone(),
            );
        } else {
            println!("is not directory: {}", &self.transfer.local_path);
        }
        true
    }

    pub async fn check_master(&self, transfer: &HdcTransferBase) {
        println!(
            "hdcfile check_master : {}",
            transfer.transfer_config.serialize().len()
        );
        let file_check_message = TaskMessage {
            channel_id: transfer.channel_id,
            command: HdcCommand::FileCheck,
            payload: transfer.transfer_config.serialize(),
        };
        let _ = BMap::put(self.transfer.session_id, file_check_message).await;
        println!("send CMD_FILE_CHECK ok");
    }

    pub async fn check_slaver(&mut self, _payload: &Vec<u8>) -> bool {
        // transferconfig deserialization
        let mut transconfig = TransferConfig::default();
        let _ = transconfig.parse(_payload.clone());
        // end deserialization

        self.transfer.file_size = transconfig.file_size;
        self.transfer.local_path = String::from(transconfig.path);
        self.transfer.is_master = false;
        println!(
            "file_size: {}, local_path:{}",
            self.transfer.file_size, self.transfer.local_path
        );

        let mut error = String::new();
        let local_path = self.transfer.local_path.clone();
        let optional_name = transconfig.optional_name.clone();
        println!("optional_name: {}", optional_name);
        if !self
            .transfer
            .check_local_path(&local_path, &optional_name, &mut error)
        {
            println!("check_local_path fail");
            return false;
        }

        if !self
            .transfer
            .check_file_name(&local_path, &optional_name, &mut error)
        {
            println!("check_file_name fail");
            return false;
        }

        let ret = self.transfer.smart_slave_path(
            &transconfig.client_cwd,
            &self.transfer.local_path,
            &transconfig.optional_name,
        );
        if ret && self.transfer.transfer_config.update_if_new {
            return false;
        }
        // open file
        let file_begin_message = TaskMessage {
            channel_id: self.transfer.channel_id,
            command: HdcCommand::FileBegin,
            payload: Vec::<u8>::new(),
        };
        let result = BMap::put(self.transfer.session_id, file_begin_message).await;
        if result.is_err() {
            return false;
        }
        println!("send CMD_FILE_BEGIN ok:{}", self.transfer.channel_id);
        true
    }

    pub fn transfer_next(&self) {}
}

#[async_trait]
impl TaskBase for HdcFile {
    fn channel_id(&self) -> u32 {
        self.transfer.channel_id()
    }

    async fn command_dispatch(
        &mut self,
        _command: HdcCommand,
        _payload: &Vec<u8>,
        _payload_size: u16,
    ) -> bool {
        println!("HdcFile::command_dispatch: {}", _command as u32);
        let mut ret = true;
        &self
            .transfer
            .command_dispatch(_command, _payload, _payload_size)
            .await;
        match _command {
            HdcCommand::FileInit => {
                let s = String::from_utf8((*_payload.clone()).to_vec());
                match s {
                    Ok(str) => {
                        ret = self.begin_transfer(&str).await;
                    }
                    Err(e) => {
                        println!("error {}", e);
                    }
                }
            }
            HdcCommand::FileCheck => {
                if !self.check_slaver(_payload).await {
                    self.task_finish();
                }
            }
            HdcCommand::FileMode => {}
            HdcCommand::FileFinish => {
                if _payload[0] == 1 {
                    if !self.transfer.task_queue.is_empty() {
                        self.transfer_next();
                    } else {
                        let _finish_message = TaskMessage {
                            channel_id: self.channel_id(),
                            command: HdcCommand::FileFinish,
                            payload: [0].to_vec(),
                        };
                        let _ = BMap::put(self.transfer.session_id, _finish_message).await;
                    }
                } else {
                    let _ = self.task_finish();
                }
            }
            _ => {
                println!("others");
            }
        }
        ret
    }

    fn stop_task(&mut self) {
        let _ = self.transfer.stop_task();
        println!("HdcFile::stop_task");
    }

    fn ready_for_release(&mut self) -> bool {
        let result = self.transfer.ready_for_release();
        println!("HdcFile::ready_for_release");
        result
    }

    fn task_finish(&self) {
        let _ = &self.transfer.task_finish();
        println!("HdcFile::task_finish");
    }
}
