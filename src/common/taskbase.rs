use super::{config::HdcCommand, hsession::TaskMessage};
use async_trait::async_trait;

#[derive(Clone, Copy, Debug)]
pub enum TaskType {
    TypeUnity,
    TypeShell,
    TypeFile,
    TypeForward,
    TypeApp,
}
#[async_trait]
pub trait TaskBase: Send + Sync + 'static {
    async fn command_dispatch(
        &mut self,
        _command: HdcCommand,
        _payload: &Vec<u8>,
        _payload_size: u16,
    ) -> bool;
    fn stop_task(&mut self);
    fn ready_for_release(&mut self) -> bool;
    fn channel_id(&self) -> u32 {
        0
    }
    fn task_finish(&self) {
        let _ = TaskMessage {
            channel_id: self.channel_id(),
            command: HdcCommand::KernelChannelClose,
            payload: [1].to_vec(),
        };
    }
}
