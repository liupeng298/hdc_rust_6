use super::hsession::TaskMessage;
use crate::common::base::Base;
use crate::common::buffer::BMap;
use crate::common::config::HdcCommand;
use crate::common::config::*;
use crate::common::taskbase::TaskBase;
use crate::serializer::transfer_struct::TransferConfig;
use async_trait::async_trait;
use std::fs::{self, File};
use std::io::prelude::*;

pub struct HdcTransferBase {
    pub need_close_notify: bool,
    pub io_index: u64,
    pub last_error: u32,
    pub is_io_finish: bool,
    pub is_master: bool,
    pub remote_path: String,
    pub local_path: String,
    pub server_or_daemon: bool,
    pub task_queue: Vec<String>,
    pub local_name: String,
    pub is_dir: bool,
    pub file_size: u64,
    pub session_id: u32,
    pub channel_id: u32,
    pub index: u64,
    pub is_file_mode_sync: bool,

    pub transfer_config: TransferConfig,
}

impl HdcTransferBase {
    pub fn new(_session_id: u32, _channel_id: u32) -> Self {
        Self {
            need_close_notify: false,
            io_index: 0,
            last_error: 0,
            is_io_finish: false,
            is_master: false,
            remote_path: String::new(),
            local_path: String::new(),
            server_or_daemon: false,
            task_queue: Vec::<String>::new(),
            local_name: String::new(),
            is_dir: false,
            file_size: 0,
            session_id: _session_id,
            channel_id: _channel_id,
            index: 0,
            is_file_mode_sync: false,
            transfer_config: TransferConfig::default(),
        }
    }

    pub fn reset_context(&mut self) {
        self.need_close_notify = false;
        self.io_index = 0;
        self.last_error = 0;
        self.is_io_finish = false;
    }

    pub fn smart_slave_path(
        &self,
        _cwd: &String,
        _local_path: &String,
        _option_name: &String,
    ) -> bool {
        true
    }

    pub fn check_local_path(
        &mut self,
        _local_path: &String,
        _optional_name: &String,
        _error: &mut String,
    ) -> bool {
        println!(
            "Checking local path localpath: {}, optional_name:{}",
            _local_path, _optional_name
        );
        if _optional_name.find(Base::get_path_sep()).is_none() {
            self.is_dir = false;
        } else {
            self.is_dir = true;
        }

        if _local_path.ends_with(Base::get_path_sep()) {
            return fs::create_dir_all(_local_path).is_ok();
        } else {
            let last = _local_path.rfind(Base::get_path_sep()).unwrap();
            println!("last: {:?}", last);
            return fs::create_dir_all(&_local_path[0..last]).is_ok()
                && File::create(_local_path).is_ok();
        }
    }

    pub fn check_file_name(
        &mut self,
        _local_path: &String,
        _option_name: &String,
        _error: &mut String,
    ) -> bool {
        println!("Checking file name");
        true
    }

    pub async fn read_and_send_data(&self) {
        println!("read_and_send_data local_path: {}", self.local_path);
        let mut file = File::open(&self.local_path).unwrap();
        const BUFFER_SIZE: usize = 20480;
        let mut buffer: [u8; BUFFER_SIZE] = [0; BUFFER_SIZE];
        let mut sum = 0;
        loop {
            let read_len = file.read(&mut buffer[..]).unwrap();
            sum += read_len;
            println!("read size:{}", sum);
            let _data_message = TaskMessage {
                channel_id: self.channel_id,
                command: HdcCommand::FileData,
                payload: buffer.to_vec(),
            };
            let _ = BMap::put(self.session_id, _data_message).await;
            if read_len < BUFFER_SIZE {
                break;
            }
        }
    }

    pub async fn recv_and_write_file(&mut self, _data: &Vec<u8>) {
        println!("recv_and_write_file {}", self.local_path);
        let mut file = File::options().append(true).open(&self.local_path).unwrap();
        let _ = file.write_all(&_data[FILE_PACKAGE_HEAD..]);
        self.index += (_data.len() - FILE_PACKAGE_HEAD) as u64;
        if self.index >= self.file_size {
            let _payload: [u8; 1] = if self.is_dir { [1] } else { [0] };
            let task_finish_message = TaskMessage {
                channel_id: self.channel_id,
                command: HdcCommand::FileFinish,
                payload: _payload.to_vec(),
            };
            let _ = BMap::put(self.session_id, task_finish_message).await;
        }
    }

    pub fn get_sub_files_resurively(&self, _path: String, _name: String) -> Vec<String> {
        Vec::<String>::new()
    }

    pub fn check_master(&self) {}

    pub fn extract_relative_path(&self, _cwd: &String, mut _path: &String) -> String {
        if !Base::is_absolute_path(_path) {
            let mut path2 = _cwd.clone();
            path2.push_str(_path);
            return path2;
        }
        _path.clone()
    }
}

#[async_trait]
impl TaskBase for HdcTransferBase {
    fn channel_id(&self) -> u32 {
        self.channel_id
    }

    async fn command_dispatch(
        &mut self,
        _command: HdcCommand,
        _payload: &Vec<u8>,
        _payload_size: u16,
    ) -> bool {
        println!(
            "HdcTransferBase::command_dispatch, _command: {:?}",
            _command as u32
        );
        match _command {
            HdcCommand::FileBegin => {
                self.read_and_send_data();
            }
            HdcCommand::FileData => {
                self.recv_and_write_file(_payload).await;
            }
            _ => println!("not valid command number"),
        }
        true
    }

    fn stop_task(&mut self) {
        println!("HdcTransferBase::stop_task");
    }
    fn ready_for_release(&mut self) -> bool {
        println!("HdcTransferBase::ready_for_release");
        true
    }
    fn task_finish(&self) {
        println!("HdcTransferBase::task_finish");
    }
}
