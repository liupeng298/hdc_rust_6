use crate::common::config::{HdcCommand, SHELL_PROC};
use crate::common::hsession::TaskMessage;
use std::collections::{HashMap, VecDeque};
use std::io::{Error, ErrorKind};
use std::sync::{Arc, Mutex};
use tokio::io;
use tokio::io::{AsyncReadExt as _, AsyncWriteExt as _};
use tokio::sync::mpsc::{self, Sender};
pub type QueueBuffer = Arc<Mutex<VecDeque<TaskMessage>>>;
pub type BufferMap = Arc<Mutex<HashMap<u32, QueueBuffer>>>;

pub struct BMap {}
impl BMap {
    fn get_instance() -> BufferMap {
        static mut MAP: Option<BufferMap> = None;
        unsafe {
            MAP.get_or_insert_with(|| Arc::new(Mutex::new(HashMap::new())))
                .clone()
        }
    }
    pub async fn get(session_id: u32) -> Option<TaskMessage> {
        let buffer_map = Self::get_instance();
        let map = buffer_map.lock().unwrap();
        if let Some(queue) = map.get(&session_id) {
            return queue.lock().unwrap().pop_front();
        }
        None
    }

    pub async fn put(session_id: u32, data: TaskMessage) -> io::Result<()> {
        let buffer_map = Self::get_instance();
        let map = buffer_map.lock().unwrap();
        if let Some(queue) = map.get(&session_id) {
            queue.lock().unwrap().push_back(data);
            return Ok(());
        }
        Err(Error::new(ErrorKind::Other, "put data error"))
    }

    pub async fn start(session_id: u32) {
        let buffer_map = Self::get_instance();
        let mut map = buffer_map.lock().unwrap();
        let arc_queue = Arc::new(Mutex::new(VecDeque::new()));
        map.insert(session_id, arc_queue);
    }
}

pub struct PtyTask {
    pub handle: tokio::task::JoinHandle<()>,
    pub tx: Sender<u8>,
}

impl PtyTask {
    pub fn new(session_id: u32, channel_id: u32) -> Self {
        let (tx, mut rx) = mpsc::channel::<u8>(100);
        let mut pty = pty_process::Pty::new().unwrap();
        let pts = pty.pts().unwrap();
        pty.resize(pty_process::Size::new(24, 80)).unwrap();
        let mut child = pty_process::Command::new(SHELL_PROC).spawn(&pts).unwrap();

        let handle = tokio::spawn(async move {
            let mut buf = [0_u8; 4096];
            loop {
                tracing::debug!("in ptytask before lock");
                tokio::select! {
                    byte = rx.recv() => {
                        pty.write_all(&[byte.unwrap()]).await.unwrap();
                    },
                    bytes = pty.read(&mut buf) => match bytes {
                        Ok(bytes) => {
                            let message = TaskMessage {
                                channel_id: channel_id,
                                command: HdcCommand::KernelEchoRaw,
                                payload: (&buf[..bytes]).to_vec(),
                            };
                            tracing::debug!("shell data put payload: {:#?}", &buf[..bytes]);
                            let _ = BMap::put(session_id, message).await.unwrap();
                        }
                        Err(e) => {
                            tracing::warn!("read pty err, channel_id={channel_id}: {}", e.to_string());
                            break;
                        }
                    },
                    _ = child.wait() => break,
                }
            }
        });
        Self { handle, tx }
    }
}

pub type PtyMapType = Arc<Mutex<HashMap<u32, Arc<PtyTask>>>>;
pub struct PtyMap {}
impl PtyMap {
    fn get_instance() -> PtyMapType {
        static mut PTYMAP: Option<PtyMapType> = None;
        unsafe {
            PTYMAP
                .get_or_insert_with(|| Arc::new(Mutex::new(HashMap::new())))
                .clone()
        }
    }

    pub async fn get(channel_id: u32) -> Option<Arc<PtyTask>> {
        let pty_map = Self::get_instance();
        let map = pty_map.lock().unwrap();
        if let Some(pty_task) = map.get(&channel_id) {
            return Some(pty_task.clone());
        }
        None
    }

    pub async fn put(channel_id: u32, pty_task: PtyTask) {
        let pty_map = Self::get_instance();
        let mut map = pty_map.lock().unwrap();
        let arc_pty_task = Arc::new(pty_task);
        map.insert(channel_id, arc_pty_task.clone());
    }

    pub async fn del(channel_id: u32) {
        let pty_map = Self::get_instance();
        let mut map = pty_map.lock().unwrap();
        map.remove(&channel_id);
    }
}
