use std::convert::TryFrom;

pub enum CompressType {
    None = 0,
    Lz4,
    Lz77,
    Lzma,
    Brotli,
}

pub enum ConnectType {
    Usb,
    Tcp,
    Bt,
}

pub enum NodeType {
    Server,
    Daemon,
}

#[derive(PartialEq, Debug, Clone, Copy)]
#[repr(u32)]
pub enum HdcCommand {
    KernelHelp = 0,
    KernelHandshake,
    KernelChannelClose,
    KernelServerKill,
    KernelTargetDiscover,
    KernelTargetList,
    KernelTargetAny,
    KernelTargetConnect,
    KernelTargetDisconnect,
    KernelEcho,
    KernelEchoRaw,
    KernelEnableKeepalive,
    KernelWakeupSlavetask,
    CheckServer,
    CheckDevice,
    WaitFor,
    // One-pass simple commands
    UnityCommandHead = 1000, // not use
    UnityExecute,
    UnityRemount,
    UnityReboot,
    UnityRunmode,
    UnityHilog,
    UnityTerminate,
    UnityRootrun,
    JdwpList,
    JdwpTrack,
    UnityCommandTail, // not use
    // It will be separated from unity in the near future
    UnityBugreportInit,
    UnityBugreportData,
    // Shell commands types
    ShellInit = 2000,
    ShellData,
    // Forward commands types
    ForwardInit = 2500,
    ForwardCheck,
    ForwardCheckResult,
    ForwardActiveSlave,
    ForwardActiveMaster,
    ForwardData,
    ForwardFreeContext,
    ForwardList,
    ForwardRemove,
    ForwardSuccess,
    // File commands
    FileInit = 3000,
    FileCheck,
    FileBegin,
    FileData,
    FileFinish,
    AppSideload,
    FileMode,
    DirMode,
    // App commands
    AppInit = 3500,
    AppCheck,
    AppBegin,
    AppData,
    AppFinish,
    AppUninstall,
    // Flashd commands
    FlashdUpdateInit = 4000,
    FlashdFlashInit,
    FlashdCheck,
    FlashdBegin,
    FlashdData,
    FlashdFinish,
    FlashdErase,
    FlashdFormat,
    FlashdProgress,
}

impl TryFrom<u32> for HdcCommand {
    type Error = ();
    fn try_from(cmd: u32) -> Result<Self, ()> {
        match cmd {
            0 => Ok(Self::KernelHelp),
            1 => Ok(Self::KernelHandshake),
            2 => Ok(Self::KernelChannelClose),
            3 => Ok(Self::KernelServerKill),
            4 => Ok(Self::KernelTargetDiscover),
            5 => Ok(Self::KernelTargetList),
            6 => Ok(Self::KernelTargetAny),
            7 => Ok(Self::KernelTargetConnect),
            8 => Ok(Self::KernelTargetDisconnect),
            9 => Ok(Self::KernelEcho),
            10 => Ok(Self::KernelEchoRaw),
            11 => Ok(Self::KernelEnableKeepalive),
            12 => Ok(Self::KernelWakeupSlavetask),
            13 => Ok(Self::CheckServer),
            14 => Ok(Self::CheckDevice),
            15 => Ok(Self::WaitFor),
            1000 => Ok(Self::UnityCommandHead),
            1001 => Ok(Self::UnityExecute),
            1002 => Ok(Self::UnityRemount),
            1003 => Ok(Self::UnityReboot),
            1004 => Ok(Self::UnityRunmode),
            1005 => Ok(Self::UnityHilog),
            1006 => Ok(Self::UnityTerminate),
            1007 => Ok(Self::UnityRootrun),
            1008 => Ok(Self::JdwpList),
            1009 => Ok(Self::JdwpTrack),
            1010 => Ok(Self::UnityCommandTail),
            1011 => Ok(Self::UnityBugreportInit),
            1012 => Ok(Self::UnityBugreportData),
            2000 => Ok(Self::ShellInit),
            2001 => Ok(Self::ShellData),
            2500 => Ok(Self::ForwardInit),
            2501 => Ok(Self::ForwardCheck),
            2502 => Ok(Self::ForwardCheckResult),
            2503 => Ok(Self::ForwardActiveSlave),
            2504 => Ok(Self::ForwardActiveMaster),
            2505 => Ok(Self::ForwardData),
            2506 => Ok(Self::ForwardFreeContext),
            2507 => Ok(Self::ForwardList),
            2508 => Ok(Self::ForwardRemove),
            2509 => Ok(Self::ForwardSuccess),
            3000 => Ok(Self::FileInit),
            3001 => Ok(Self::FileCheck),
            3002 => Ok(Self::FileBegin),
            3003 => Ok(Self::FileData),
            3004 => Ok(Self::FileFinish),
            3005 => Ok(Self::AppSideload),
            3006 => Ok(Self::FileMode),
            3007 => Ok(Self::DirMode),
            3500 => Ok(Self::AppInit),
            3501 => Ok(Self::AppCheck),
            3502 => Ok(Self::AppBegin),
            3503 => Ok(Self::AppData),
            3504 => Ok(Self::AppFinish),
            3505 => Ok(Self::AppUninstall),
            4000 => Ok(Self::FlashdUpdateInit),
            4001 => Ok(Self::FlashdFlashInit),
            4002 => Ok(Self::FlashdCheck),
            4003 => Ok(Self::FlashdBegin),
            4004 => Ok(Self::FlashdData),
            4005 => Ok(Self::FlashdFinish),
            4006 => Ok(Self::FlashdErase),
            4007 => Ok(Self::FlashdFormat),
            4008 => Ok(Self::FlashdProgress),
            _ => Err(()),
        }
    }
}

pub enum AuthType {
    None,
    Token,
    Signature,
    Publickey,
    OK,
}

pub const PACKET_FLAG: &[u8] = "HW".as_bytes();
pub const VER_PROTOCOL: u16 = 1;
pub const ENABLE_IO_CHECK: bool = false;
pub const PAYLOAD_VCODE: u8 = 0x09;
pub const HDC_BUF_MAX_SIZE: usize = 0x7fffffff;
pub const HANDSHAKE_MESSAGE: &str = "OHOS HDC";
pub const FILE_PACKAGE_HEAD: usize = 64;

pub const SHELL_PROC: &str = "sh";
pub const HOSTNAME_FILE: &str = "/proc/sys/kernel/hostname";
