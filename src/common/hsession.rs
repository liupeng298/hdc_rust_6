use crate::common::config::{ConnectType, HdcCommand, NodeType};
use crate::common::taskbase::TaskBase;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Debug)]
pub struct TaskMessage {
    pub channel_id: u32,
    pub command: HdcCommand,
    pub payload: Vec<u8>,
}

pub struct HdcSession {
    pub session_id: u32,
    pub ref_cnt: u32,
    pub connect_key: String,
    pub node_type: NodeType,
    pub connect_type: ConnectType,
    pub io_buf: Vec<u8>,
    pub vote_reset: bool,
    pub map_tasks: HashMap<u32, Arc<Mutex<dyn TaskBase>>>,
}

impl HdcSession {
    pub fn new(
        session_id: u32,
        connect_key: String,
        node_type: NodeType,
        connect_type: ConnectType,
    ) -> Self {
        Self {
            session_id,
            ref_cnt: 0,
            connect_key,
            node_type,
            connect_type,
            io_buf: Vec::new(),
            vote_reset: false,
            map_tasks: HashMap::new(),
        }
    }
}

impl Default for HdcSession {
    fn default() -> Self {
        Self {
            session_id: 0,
            ref_cnt: 0,
            connect_key: "".to_string(),
            node_type: NodeType::Server,
            connect_type: ConnectType::Tcp,
            io_buf: Vec::new(),
            vote_reset: false,
            map_tasks: HashMap::new(),
        }
    }
}

impl HdcSession {}

pub type HSession = Arc<Mutex<HdcSession>>;
pub type HSessionMap = Arc<Mutex<HashMap<u32, HSession>>>;

struct HSMap {}
impl HSMap {
    fn get_instance() -> HSessionMap {
        static mut HMAP: Option<HSessionMap> = None;
        unsafe {
            HMAP.get_or_insert_with(|| Arc::new(Mutex::new(HashMap::new())))
                .clone()
        }
    }
}

pub enum ActionType {
    Add(HdcSession),
    Remove(u32),
    Query(u32),
    QueryRef(u32),
    Update(u32, HdcSession),
    VoteReset(u32),
}

pub async fn admin_session(action: ActionType) -> Option<HSession> {
    let hsession_map = HSMap::get_instance();
    match action {
        ActionType::Add(hsession) => {
            let session_id = hsession.session_id;
            let mut map = hsession_map.lock().await;
            let arc_session = Arc::new(Mutex::new(hsession));
            map.insert(session_id, arc_session.clone());
            return Some(arc_session);
        }
        ActionType::Remove(session_id) => {
            let mut map = hsession_map.lock().await;
            map.remove(&session_id);
        }
        ActionType::Query(session_id) => {
            let map = hsession_map.lock().await;
            if let Some(hsession) = map.get(&session_id) {
                return Some(hsession.clone());
            }
        }
        ActionType::QueryRef(session_id) => {
            let map = hsession_map.lock().await;
            if let Some(hsession) = map.get(&session_id) {
                hsession.lock().await.ref_cnt += 1;
                return Some(hsession.clone());
            }
        }
        ActionType::Update(session_id, hsession) => {
            let mut map = hsession_map.lock().await;
            map.remove(&session_id);
            map.insert(hsession.session_id, Arc::new(Mutex::new(hsession)));
        }
        ActionType::VoteReset(session_id) => {
            let map = hsession_map.lock().await;
            let mut need_reset = true;
            let mut found = false;

            for (sid, hsession) in map.iter() {
                if *sid == session_id {
                    hsession.lock().await.vote_reset = true;
                    found = true;
                } else if hsession.lock().await.vote_reset == false {
                    need_reset = false;
                }
            }
            if found && need_reset {
                std::process::abort();
            }
        }
    }
    None
}

pub async fn malloc_session(
    node_type: NodeType,
    connect_type: ConnectType,
    session_id: u32,
) -> Option<HSession> {
    let seid = if session_id == 0 {
        get_pseudo_sessionid().await
    } else {
        session_id
    };
    let hsession = HdcSession::new(seid, "".to_string(), node_type, connect_type);
    admin_session(ActionType::Add(hsession)).await
}

async fn get_pseudo_sessionid() -> u32 {
    use std::time::{SystemTime, UNIX_EPOCH};
    loop {
        let seid = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis() as u32;
        let opt = admin_session(ActionType::Query(seid)).await;
        if opt.is_none() {
            return seid;
        }
    }
}
