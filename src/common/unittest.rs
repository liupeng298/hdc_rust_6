#[cfg(test)]
mod session_test {
    use tokio;

    use crate::common::config::{ConnectType, NodeType};
    use crate::common::hsession;
    use crate::common::hsession::{ActionType, HdcSession};

    #[tokio::test]
    async fn if_hsession_query_work() {
        let t1 = tokio::spawn(async {
            let hs = HdcSession::new(
                111,
                "test_key".to_string(),
                NodeType::Daemon,
                ConnectType::Tcp,
            );
            hsession::admin_session(ActionType::Add(hs)).await;
        });

        let t2 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
            let opt = hsession::admin_session(ActionType::Query(111)).await;
            let lock = opt.unwrap();
            let hs = lock.lock().await;
            assert_eq!(hs.connect_key, "test_key".to_string());
        });

        t1.await.unwrap();
        t2.await.unwrap();
    }

    #[tokio::test]
    async fn if_hsession_query_ref_work() {
        let t1 = tokio::spawn(async {
            let hs = HdcSession::new(
                222,
                "test_key".to_string(),
                NodeType::Daemon,
                ConnectType::Tcp,
            );
            hsession::admin_session(ActionType::Add(hs)).await;
        });

        let t2 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
            let _ = hsession::admin_session(ActionType::QueryRef(222)).await;
        });

        let t3 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(200)).await;
            let opt = hsession::admin_session(ActionType::Query(222)).await;
            let lock = opt.unwrap();
            let hs = lock.lock().await;
            assert_eq!(hs.ref_cnt, 1);
        });

        t1.await.unwrap();
        t2.await.unwrap();
        t3.await.unwrap();
    }

    #[tokio::test]
    async fn if_hsession_update_work() {
        let t1 = tokio::spawn(async {
            let hs = HdcSession::new(
                333,
                "test_key".to_string(),
                NodeType::Daemon,
                ConnectType::Tcp,
            );
            hsession::admin_session(ActionType::Add(hs)).await;
        });

        let t2 = tokio::spawn(async {
            let hs = HdcSession::new(
                3333,
                "test_key2".to_string(),
                NodeType::Server,
                ConnectType::Bt,
            );
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(300)).await;
            let _ = hsession::admin_session(ActionType::Update(333, hs)).await;
        });

        let t3 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(400)).await;
            let opt = hsession::admin_session(ActionType::Query(3333)).await;
            let lock = opt.unwrap();
            let hs = lock.lock().await;
            assert_eq!(hs.connect_key, "test_key2".to_string());
            assert!(matches!(hs.connect_type, ConnectType::Bt));
            assert!(matches!(hs.node_type, NodeType::Server));
        });

        let t4 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(500)).await;
            let opt = hsession::admin_session(ActionType::Query(333)).await;
            assert!(opt.is_none());
        });

        t1.await.unwrap();
        t2.await.unwrap();
        t3.await.unwrap();
        t4.await.unwrap();
    }

    #[tokio::test]
    async fn if_hsession_update_outside_admin_work() {
        let t1 = tokio::spawn(async {
            let hs = HdcSession::new(
                444,
                "test_key".to_string(),
                NodeType::Daemon,
                ConnectType::Tcp,
            );
            hsession::admin_session(ActionType::Add(hs)).await;
        });

        let t2 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(200)).await;
            let opt = hsession::admin_session(ActionType::Query(444)).await;
            let lock = opt.unwrap();
            let mut hs = lock.lock().await;
            hs.connect_key = "new_key".to_string();
        });

        let t3 = tokio::spawn(async {
            let _ = tokio::time::sleep(tokio::time::Duration::from_millis(300)).await;
            let opt = hsession::admin_session(ActionType::Query(444)).await;
            let lock = opt.unwrap();
            let hs = lock.lock().await;
            assert_eq!(hs.connect_key, "new_key".to_string());
        });

        t1.await.unwrap();
        t2.await.unwrap();
        t3.await.unwrap();
    }
}

// #[cfg(test)]
// mod file_test {
//     use crate::common;
//     use crate::serializer;

//     use common::{config::HdcCommand, hdcfile::HdcFile, taskbase::TaskBase};
//     use serializer::serialize::Serialization;
//     use std::fs::File;
//     use std::io::prelude::*;
//     #[test]
//     fn test_file_task() {
//         let mut hdcfile = HdcFile::new(111, 1001);
//         // master use
//         hdcfile.BeginTransfer(&"-cwd \"/home/\" /bin/mkdir /home/mkdir".to_string());

//         // slaver use
//         let payload = hdcfile.transfer.transfer_config.serialize();
//         hdcfile.command_dispatch(HdcCommand::FileCheck, &payload, payload.len() as u16);

//         // master use
//         // hdcfile.command_dispatch(HdcCommand::FileBegin as u32, &Vec::<u8>::new(), 0);

//         // slaver use
//         let mut file = File::open(String::from("/bin/mkdir")).unwrap();
//         const BUFFER_SIZE: usize = 20480;
//         let mut buffer: [u8; BUFFER_SIZE] = [0; BUFFER_SIZE];
//         let mut error = false;
//         let mut sum = 0;
//         loop {
//             let read_len = file.read(&mut buffer[..]).unwrap();
//             if error {
//                 break;
//             }

//             sum += read_len;
//             println!("read size:{}, sum:{}", read_len, sum);
//             hdcfile.command_dispatch(
//                 HdcCommand::FileData,
//                 &buffer[0..read_len].to_vec(),
//                 read_len as u16,
//             );
//             if read_len < BUFFER_SIZE {
//                 break;
//             }
//         }
//         let payload = [1].to_vec();
//         hdcfile.command_dispatch(HdcCommand::FileFinish, &payload, 1);

//         let dest_file = File::open(String::from("/home/mkdir")).unwrap();
//         assert_eq!(
//             file.metadata().unwrap().len(),
//             dest_file.metadata().unwrap().len()
//         );
//     }
// }
