pub struct Base {}

impl Base {
    // -cwd "C:\Users\liupeng\" C:\Users\liupeng\Desktop\hdcfile\hdcd_system /data/hdcd
    pub fn split_command_to_args(command_line: &String, count: &mut u32) -> Vec<String> {
        let mut argv: Vec<String> = Vec::new();
        let mut argc = 0;
        let _a = 0;
        let mut is_quoted = false;
        let mut is_text = false;
        let mut is_space = true;

        let len = command_line.len();
        if len < 1 {
            return Vec::new();
        }
        argv.push(String::new());
        for _a in command_line.chars() {
            if is_quoted {
                if _a == '\"' {
                    is_quoted = false;
                } else {
                    argv.last_mut().unwrap().push(_a);
                }
            } else {
                match _a {
                    '\"' => {
                        is_quoted = true;
                        is_text = true;
                        if is_space {
                            argc += 1;
                        }
                        is_space = false;
                    }
                    x if x == ' ' || x == '\t' || x == '\n' || x == '\r' => {
                        if is_text {
                            argv.push(String::new());
                        }
                        is_text = false;
                        is_space = true;
                    }
                    _ => {
                        is_text = true;
                        if is_space {
                            argc += 1;
                        }
                        argv.last_mut().unwrap().push(_a);
                        is_space = false;
                    }
                }
            }
        }

        *count = argc;
        argv
    }

    pub fn get_path_sep() -> char {
        if cfg!(target_os = "windows") {
            return '\\';
        } else {
            return '/';
        }
    }

    pub fn is_absolute_path(path: &String) -> bool {
        path.starts_with(Self::get_path_sep())
    }

    pub fn get_full_file_path(s: &mut String) -> Option<String> {
        println!("get_full_file_path s: {}", s);
        let temp = s.clone();
        let chars: std::str::Chars<'_> = temp.chars();
        let mut result = String::new();
        let mut len = chars.clone().count() - 1;
        while len >= 0 && chars.clone().nth(len) == Some(Self::get_path_sep()) {
            len -= 1;
        }
        println!("len:{}", len);
        let begin = len;
        while len >= 0 && chars.clone().nth(len) != Some(Self::get_path_sep()) {
            len -= 1;
        }
        println!("len2:{}", len);
        for i in len + 1..begin + 1 {
            result.push(chars.clone().nth(i).unwrap());
        }
        Some(result)
    }
}
