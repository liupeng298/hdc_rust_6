extern crate libc;

mod common;
mod serializer;

// use tracing::Level;

// fn trace_log() {
//     let subscriber = tracing_subscriber::fmt()
//         .compact()
//         .with_max_level(Level::DEBUG)
//         .with_file(true)
//         .with_line_number(true)
//         .with_thread_ids(true)
//         .with_target(false)
//         .finish();
//     tracing::subscriber::set_global_default(subscriber).unwrap();
// }

#[tokio::main]
async fn main() {
    // trace_log();
    let _ = common::tcp::daemon_start().await;
}
