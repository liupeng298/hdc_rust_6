use crate::common::config::*;
use crate::serializer::serialize::Serialization;
use crate::serializer::{pack_struct, session_struct};
use std::io::{Error, ErrorKind};
use std::mem;
use tokio::io;

pub const HEAD_SIZE: usize = mem::size_of::<pack_struct::PayloadHeadPack>();

fn calc_check_sum(data: &Vec<u8>) -> u8 {
    data.iter().sum()
}

pub fn unpack_payload_head(data: Vec<u8>) -> io::Result<session_struct::PayloadHead> {
    let mut payload_head = session_struct::PayloadHead::default();
    let _ = payload_head.parse(data)?;

    if payload_head.flag != PACKET_FLAG {
        return Err(Error::new(ErrorKind::Other, "PACKET_FLAG incorrect"));
    }
    Ok(payload_head)
}

pub fn unpack_payload_protect(data: Vec<u8>) -> io::Result<session_struct::PayloadProtect> {
    let mut payload_protect = session_struct::PayloadProtect::default();
    let _ = payload_protect.parse(data)?;
    if payload_protect.v_code != PAYLOAD_VCODE {
        return Err(Error::new(
            ErrorKind::Other,
            "Session recv static vcode failed",
        ));
    }
    Ok(payload_protect)
}

pub fn unpack_payload(
    data: Vec<u8>,
    command_flag: u32,
    check_sum: u8,
) -> io::Result<session_struct::SessionHandShake> {
    if ENABLE_IO_CHECK && calc_check_sum(&data) != check_sum {
        return Err(Error::new(
            ErrorKind::Other,
            "Session recv CalcCheckSum failed",
        ));
    }

    match HdcCommand::try_from(command_flag) {
        Ok(HdcCommand::KernelHandshake) => {
            let mut handshake = session_struct::SessionHandShake::default();
            let _ = handshake.parse(data)?;
            return Ok(handshake);
        }
        _ => Err(Error::new(ErrorKind::Other, "unknown command flag")),
    }
}

pub fn concat_pack(channel_id: u32, command_flag: HdcCommand, data: Vec<u8>) -> Vec<u8> {
    // let data = obj.serialize();
    let check_sum: u8 = if ENABLE_IO_CHECK {
        calc_check_sum(&data)
    } else {
        0
    };
    let payload_protect = session_struct::PayloadProtect {
        channel_id,
        command_flag: command_flag as u32,
        check_sum,
        v_code: PAYLOAD_VCODE,
    };

    let protect_buf = payload_protect.serialize();

    let payload_head = session_struct::PayloadHead {
        flag: [PACKET_FLAG[0], PACKET_FLAG[1]],
        protocol_ver: VER_PROTOCOL as u8,
        head_size: (protect_buf.len() as u16).to_be(),
        data_size: (data.len() as u32).to_be(),
        reserve: [0, 0],
    };

    tracing::info!(
        "concat pack, datasize: {}, slicelen: {}, headsize: {}",
        data.len(),
        data.as_slice().len(),
        protect_buf.len()
    );

    let head_buf = payload_head.serialize();
    let pack = [head_buf.as_slice(), protect_buf.as_slice(), data.as_slice()].concat();
    return pack;
}
