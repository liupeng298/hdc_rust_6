#[derive(Debug, Default)]
pub struct TransferConfig {
    pub file_size: u64,
    pub atime: u64,
    pub mtime: u64,
    pub options: String,
    pub path: String,
    pub optional_name: String,
    pub update_if_new: bool,
    pub compress_type: u8,
    pub hold_timestamp: bool,
    pub function_name: String,
    pub client_cwd: String,
    pub reserve1: String,
    pub reserve2: String,
}

#[derive(Debug, Default)]
pub struct FileMode {
    pub perm: u64,
    pub u_id: u64,
    pub g_id: u64,
    pub context: String,
    pub full_name: String,
}

#[derive(Debug, Default)]
pub struct TransferPayload {
    pub index: u64,
    pub compress_type: u8,
    pub compress_size: u32,
    pub uncompress_size: u32,
}
