#[derive(Debug, Default)]
pub struct SessionHandShake {
    pub banner: String,
    pub auth_type: u8,
    pub session_id: u32,
    pub connect_key: String,
    pub buf: String,
    pub version: String,
}

#[derive(Debug, Default)]
pub struct PayloadProtect {
    pub channel_id: u32,
    pub command_flag: u32,
    pub check_sum: u8,
    pub v_code: u8,
}

#[derive(Debug, Default)]
pub struct PayloadHead {
    pub flag: [u8; 2],
    pub reserve: [u8; 2],
    pub protocol_ver: u8,
    pub head_size: u16,
    pub data_size: u32,
}
