#[cfg(test)]
mod tests {
    use crate::serializer::serialize::Serialization;
    use crate::serializer::{session_struct, transfer_struct};

    #[test]
    fn if_session_hand_shake_works() {
        let send = session_struct::SessionHandShake {
            banner: "test_banner".to_string(),
            auth_type: 1,
            session_id: 2,
            connect_key: "test_connect_key".to_string(),
            buf: "test_buf".to_string(),
            version: "test_version".to_string(),
        };

        let serialized = send.serialize();
        let mut recv = session_struct::SessionHandShake::default();
        let suc = recv.parse(serialized);

        println!("{:#?}", recv);
        assert!(suc.is_ok());
        assert_eq!(recv.banner.as_str(), send.banner.as_str());
        assert_eq!(recv.connect_key.as_str(), send.connect_key.as_str());
        assert_eq!(recv.buf.as_str(), send.buf.as_str());
        assert_eq!(recv.version.as_str(), send.version.as_str());
        assert_eq!(recv.auth_type, send.auth_type);
        assert_eq!(recv.session_id, send.session_id);
    }

    #[test]
    fn if_session_transfer_payload_works() {
        let send = transfer_struct::TransferPayload {
            index: 1 << 60,
            compress_type: 1 << 6,
            compress_size: 1 << 20,
            uncompress_size: 1 << 23,
        };

        let serialized = send.serialize();
        let mut recv = transfer_struct::TransferPayload::default();
        let suc = recv.parse(serialized);

        println!("{:#?}", recv);
        assert!(suc.is_ok());
        assert_eq!(recv.index, send.index);
        assert_eq!(recv.compress_type, send.compress_type);
        assert_eq!(recv.compress_size, send.compress_size);
        assert_eq!(recv.uncompress_size, send.uncompress_size);
    }

    #[test]
    fn if_transfer_config_works() {
        let send = transfer_struct::TransferConfig {
            file_size: 1 << 40,
            atime: 1 << 50,
            mtime: 1 << 60,
            options: "options".to_string(),
            path: "path".to_string(),
            optional_name: "optional_name".to_string(),
            update_if_new: true,
            compress_type: 3,
            hold_timestamp: false,
            function_name: "function_name".to_string(),
            client_cwd: "client_cwd\\client_cwd".to_string(),
            reserve1: "reserve1".to_string(),
            reserve2: "reserve2".to_string(),
        };

        let serialized = send.serialize();
        let mut recv = transfer_struct::TransferConfig {
            ..Default::default()
        };
        let suc = recv.parse(serialized);

        println!("{:#?}", recv);
        assert!(suc.is_ok());
        assert_eq!(recv.options.as_str(), send.options.as_str());
        assert_eq!(recv.path.as_str(), send.path.as_str());
        assert_eq!(recv.optional_name.as_str(), send.optional_name.as_str());
        assert_eq!(recv.function_name.as_str(), send.function_name.as_str());
        assert_eq!(recv.client_cwd.as_str(), send.client_cwd.as_str());
        assert_eq!(recv.reserve1.as_str(), send.reserve1.as_str());
        assert_eq!(recv.reserve2.as_str(), send.reserve2.as_str());
        assert_eq!(recv.file_size, send.file_size);
        assert_eq!(recv.atime, send.atime);
        assert_eq!(recv.mtime, send.mtime);
        assert_eq!(recv.update_if_new, send.update_if_new);
        assert_eq!(recv.hold_timestamp, send.hold_timestamp);
        assert_eq!(recv.compress_type, send.compress_type);
    }

    #[test]
    fn if_session_payload_head_works() {
        let send = session_struct::PayloadHead {
            flag: [1, 2],
            reserve: [3, 4],
            protocol_ver: 0x11,
            head_size: 0x22,
            data_size: 0x33,
        };
        let serialized = send.serialize();
        let mut recv = session_struct::PayloadHead::default();
        let suc = recv.parse(serialized);

        println!("{:#?}", recv);
        assert!(suc.is_ok());
        assert_eq!(recv.flag, send.flag);
    }
}
