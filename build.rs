extern crate cc;

use std::env;
use std::path::Path;

fn main() {
    let oh_path = env::var_os("OH_DIR").unwrap();
    let include_path = Path::new(&oh_path).join("third_party/bounds_checking_function/include");
    let memcpy_dep =
        Path::new(&oh_path).join("third_party/bounds_checking_function/src/memcpy_s.c");
    let memset_dep =
        Path::new(&oh_path).join("third_party/bounds_checking_function/src/memset_s.c");

    cc::Build::new()
        .include(include_path)
        .file(memcpy_dep)
        .file(memset_dep)
        .file("src/serializer/serial_struct.cpp")
        .cpp(true)
        .flag("-std=c++17")
        .compile("serial_struct.so");

}
